import React from 'react';
import ReactDOM from 'react-dom';
import reduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import App from './App';
import '../src/assets/style/style.css';
import searchReducer from './reducers/SearchReducer';

const store = createStore( searchReducer, {},  applyMiddleware(reduxThunk) );

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.querySelector('#search-component-holder'));

