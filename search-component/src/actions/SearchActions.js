import axios from 'axios';

import {
    SEARCH_COUNTRIES_RESULT,
} from './types';


export const searchCountries = (inputValue) => {
    return (async(dispatch) => {
        const res = await axios.get('https://code.totaralms.com/countries-json.php?search='+inputValue+'&limit=195');
        dispatch({type: SEARCH_COUNTRIES_RESULT, payload: res.data.results})
    })
};
