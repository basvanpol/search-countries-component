import React from 'react';

const Input = (props) => {
    return(
        <input onChange={props.changed} placeholder={props.placeHolderText}/>
    )
};

export default Input;