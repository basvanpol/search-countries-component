import React from 'react';

import listClasses from '../../src/components/lists.css';

const list = (props) => {
    return (
        <ul className={listClasses.List}>
            {props.data ? props.data.map((item, index) => {
                    return (
                        <li
                            className={[listClasses.ListItem, listClasses.small].join(' ')}
                            key={index}>
                        <span>
                            {item[props.dataKey]}
                        </span>
                        </li>
                    )
                })
                : null}
        </ul>
    )
};

export default list;