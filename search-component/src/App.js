import React, {Component} from 'react';
import {connect} from 'react-redux';
import {searchCountries} from './actions/SearchActions';
import Input from './components/Input';
import List from './components/List';
import iconClasses from '../src/assets/style/icons.css';
import formClasses from '../src/components/forms.css';
import listClasses from '../src/components/lists.css';

class App extends Component {

    inputChangedHandler = (event) => {
        const searchValue = event.target.value;
        this.props.searchCountries(searchValue);
    };


    render() {
        return (
            <div>
                <div className={formClasses.InputContainer}>
                    <Input changed={(event) => this.inputChangedHandler(event)}
                           placeHolderText="Search countries.."/>
                    <span>
                        <i className={[iconClasses.faSearch, iconClasses.fa].join(' ')}></i>
                    </span>
                </div>
                <div className={listClasses.ListContainer}>
                    <List data={this.props.countries} dataKey="country">
                    </List>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        countries: state.countries
    }
};

export default connect(mapStateToProps, {searchCountries})(App);
