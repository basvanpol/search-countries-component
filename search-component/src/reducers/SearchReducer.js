import { SEARCH_COUNTRIES_RESULT } from '../actions/types';

const searchReducer = (state = {}, action) => {
    switch(action.type){
        case SEARCH_COUNTRIES_RESULT:
            return {
                countries: action.payload
            }
        default:
            return state
    }
};

export default searchReducer;